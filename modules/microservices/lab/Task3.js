$(window).load(function () {
    // IGNORE THIS: NO CHANGES NEEDED HERE
    var value="100000";
    $("#final_balance").val(value);
})
 

function microservice_transfer() {
    /* The main microservice function: Your code goes here */

    /* Your task is to create a microservice, that given an amount to transfer
    and the time of day, calculates the most appropriate mode of transfer, as per 
    the conditions given in the HTML file. */
    
    var amount = parseInt($("#amount").val());
    var method = $("#mode").val();
    var balance = parseInt($("#final_balance").val());

    var today = new Date();
    var hour = today.getHours(); // To calculate the current hour

    // If there's not enough balance left
    if (amount>balance)
        alert("Insufficient funds");

    // Enter code to calculate most appropriate mode of transfer

    /* Now make a POST request to the appropriate endpoint, based on the method
    calculated in the previous step. If your method is RTGS or NEFT, calculate 
    the time left for transaction to complete, store it in message and set
    this message as the HTML of the div with id 'status' */

}