function microservice_balance() {
    
    // The primary microservice function: Your code goes here
    
    
    /* Your first subtask is to create an API endpoint /summary that takes details
     from /savings, /ppf, /deposit and /demat and totals them into an account
     summary (hint: check app.py to create an API endpoint) */

    var string = ""; 
    /* After the creation of this route, make an AJAX call to /summary and store the output
    in the variable named string, and set the HTML of div with ID 'status' to the contents of 
    string as shown below */ 

    $("#status").html(string);
}