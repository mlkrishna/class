// click a button and a function will run 
document.getElementById("ident").onclick = useID;
document.getElementById("tag").onclick = useTag;
document.getElementById("class").onclick = useClass;

// 3 functions, one for each button -- 

// one p has the ID "first" - so only that p will change
function useID() {
  var sampleMessage = "Changed para";
  document.getElementById("first").innerHTML = sampleMessage;
}

// there are two h2 elements - they change when you click this
// only h2 text will change
function useTag() {
  var sampleMessage = "Changed tag";
  var heds = document.getElementsByTagName("h2");
  for (var i = 0; i < heds.length; i++) {
    heds[i].innerHTML = sampleMessage;
  }
}

// two elements have the class "foo" - they change when 
// you click this
function useClass() {
  var sampleMessage = "changedClass"
  var allFoos = document.getElementsByClassName("foo");
  for (var i = 0; i < allFoos.length; i++) {
    allFoos[i].innerHTML = sampleMessage;
  }
}
