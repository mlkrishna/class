#+title:  Examples of Functional Programs in Racket
#+author:  Venkatesh Choppella
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t broken-links:nil c:nil
#+OPTIONS: creator:nil d:(not "LOGBOOK") date:t e:t
#+OPTIONS: email:nil f:t inline:t num:t p:nil pri:nil
#+OPTIONS: prop:nil stat:t tags:t tasks:t tex:t timestamp:t
#+OPTIONS: title:t toc:nil todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.3.2 (Org mode 9.1.9)

* Introduction
This note includes some of the examples I did in class to
demonstrate some aspects of functional programming:
Recursion and Tail recursion, and higher order functions.

The [[./ex.rkt][source code]] is available.  To run it, you will need to
dowload the [[https://www.racket-lang.org][Racket]] programming language installation.  


** Boilerplate
#+BEGIN_SRC scheme :tangle ex.rkt
#lang racket
(require racket/list)
(provide (all-defined-out))
(require rackunit)

#+END_SRC

* Functional Programs run by substitution and simplification

One advantage of functional programs is that they run by
substitution and simplification.  

  1. Replace identifier with its  definition. 
  2. Simplify function applications after binding arguments
     to formals.
  3. Carry out algebraic simplification.

  4. Repeat until the answer can not be reduced further. 

** Example
Consider the function 
#+BEGIN_SRC scheme :tangle ex.rkt
(define f
  (lambda (x)
    (+ x 3)))

(define g
  (lambda (y)
    (* y 2)))
#+END_SRC

To `run' the expression =(+ (f 2) (g 5))=, we evaluate it as
follows,
#+BEGIN_EXAMPLE
(+ (f 2) (g 5))  = 

(+ ((lambda (x) (+ x 3)) 2) (g 5)) = 
(+ (+ 3 2) (g 5)) = 
(+ 5 (g 5)) = 
(+ 5 ((lambda (y) (* y 2)) 5)) = 
(+ 5 (* 5 2)) =
(+ 5 10) = 
15
#+END_EXAMPLE

The substutions can be done in any order, even in parallel!
The advantage is that running a program is like simplifying
a high-school mathematics formula. 

* Recursion and stack
One might ask what happens to things like stacks when we run
functional programs.   The equivalent of a stack is runtime
context, that keeps building when get deeper into recursion
(by a recursive call), and shrinks when we get out of
recursion (when we return from the call). 

Consider the following recursive program that computes the
factorial of a natural number. 
#+BEGIN_SRC scheme :tangle ex.rkt
(define facRec
  (lambda (n)
    (if (= n 0)
        1
        (* n (facRec (- n 1))))))
#+END_SRC

The execution of  =(facRec 3)= is shown below.    The
execution is a series of expressions each obtained from the
previous after simplification.  Notice the
growth in the dynamic (execution) context before it
shrinks.  
#+BEGIN_EXAMPLE
(facRec 3) =
(* 3 (facRec 2)) =
(* 3 (* 2 (facRec 1))) =
(* 3 (* 2 (* 1 (facRec 0)))) =
(* 3 (* 2 (* 1 1))) =
(* 3 (* 2 1)) =
(* 3 2) =
6
#+END_EXAMPLE


* Tail Recursion
It is possible to have a function call in a position inside
a function such that the function call does not grow the
execution context.   Consider an alternative implementation
of the factorial function.    

#+BEGIN_SRC scheme :tangle 
(define facIter
  (lambda (n)
    (helper n 1)))

(define helper
  (lambda (i a)
    (if (= i 0)
        a
        (helper (- i 1) (* a i)))))
#+END_SRC

Notice the two calls to =helper= in the above code.  In each
case the result of the call is returned as the result of the
function.  Whenever the result of a subexpression is the
result of the entire function, such an expression is said to
be in /tail/ position.  For example, in the above, in the
definition of =helper=, both the expression =a= and the call
to =helper= are in tail position.  If the expression in the
tail position is a call, then it is called a tail call.  If
the call happens to be a tail call, then we have an instance
of tail recursion.  A function is /tail recursive/ if it is
recursive, and all its recursive calls are tail calls.


Let us now examine the runtime footprint of tail recursion.
Consider =(facIter 3)=:

#+BEGIN_EXAMPLE
(facIter 3) =
(helper 3 1) =
(helper 2 3) =
(helper 1 6) =
(helper 0 6) =
6
#+END_EXAMPLE

Notice now that there is recursion, but the stack doesn't
grow!.  This is an illustration that tail recursive calls do
not grow the stack, and are therefore, for all practical
purpose equivalent to iterations.  Some languages, like
Racket and Scala (with some restrictions) guarantee that
tail recursion will be implemented as iteration.
Unfortunately, many modern languages still do not make such
guarantees.  As a result, tail recursion is not encouraged
in languages Python and Javascript and Java. 

* =Reduce=  higher-order function 

One big advantage of functional programs is that they save
duplication of code. 

Consider the following programs:
;;; sums a list of numbers

#+BEGIN_SRC scheme :tangle ex.rkt
(define sum-list
  (lambda (ls) ;; ls is a list of numbers
    (if (null? ls)
        0
        (+ (first ls)
           (sum-list (rest ls))))))

;;; unit tests
(check-equal? (sum-list '(1 2 4 3)) 10)
(check-equal?  (sum-list '()) 0)

(define multiply-list
  (lambda (ls) ;; ls is a list of numbers
    (if (null? ls)
        1
        (* (first ls)
           (multiply-list (rest ls))))))

;;; unit tests
(check-equal? (multiply-list '(1 2 4 3)) 24)
(check-equal?  (multiply-list '()) 1)

#+END_SRC

They both do very similar things.  Could we not write one
program and synthesize the other two programs by simply
feeding different parameters: =+= and =0= in one case and
=*= and =1= in the other case? 

Functional Programming makes this possible by encouraging
you to write /higher-order functions/, functions that either
take as arguments other functions, or return functions. 

Here is a popular function: =reduce-list= (of Map Reduce fame)
that generalizes the above two functions: 

#+BEGIN_SRC scheme :tangle :ex.rkt
(define reduce-list
  (lambda  (ls base op)
    (if (null? ls)
        base
        (op (first ls)
           (reduce-list (rest ls) base op)))))
#+END_SRC

The two earlier functions may now be synthesised as follows:

#+BEGIN_SRC scheme :tangle 
(define sum-list2
  (lambda (ls)
    (reduce-list ls 0 +)))

(check-equal? (sum-list2 '(1 2 4 3) 10))
(check-equal?  (sum-list2 '() 0))

(define multiply-list2
  (lambda (ls)
    (reduce-list ls 1 *)))

(check-equal? (multiply-list2 '(1 2 4 3) 24))
(check-equal?  (multiply-list2 '() 1))

#+END_SRC

Note that =reduce-list= is higher-order because it takes
=op= which is a function.  

Another example is the =compose= function that takes two
functions and returns their composition.  Note that
=compose= not only takes functions but also returns a
function.

#+BEGIN_SRC scheme :tangle ex.rkt
;;; ((compose f g) x) = (f (g x))
(define compose
  (lambda (f g) 
    (lambda (x)
      (f (g x)))))
#+END_SRC

Here is an example of using =compose=:

#+BEGIN_SRC scheme :tangle ex.rkt
(define *9/5
  (lambda (t)
    (* t (/ 9 5))))

(define add-32
  (lambda (t)
    (+ t 32)))

(define c->f
  (compose add-32 *9/5))

(check-equal? (c->f 0) 32)
(check-equal? (c->f 100) 212)
#+END_SRC


